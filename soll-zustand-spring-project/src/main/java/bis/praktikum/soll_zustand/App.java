package bis.praktikum.soll_zustand;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@ComponentScan({"controller"})
public class App {
	
    public static void main( String[] args ) throws Exception {
    	SpringApplication.run(App.class, args);        
    }
}
