package controller;

import javax.validation.Valid;
import model.WebForm;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Controller 
public class WebController extends WebMvcConfigurerAdapter {
	
	@Override 
	public void addViewControllers(ViewControllerRegistry vcr){
		vcr.addViewController("/results").setViewName("results");
	}
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String showWebForm(WebForm webForm) {
		return "home";
	}
	
	@RequestMapping(value="/", method=RequestMethod.POST)
	public String checkPersonInfo(@Valid WebForm webForm, BindingResult bindingResult){
		if (bindingResult.hasErrors()){
			return "home";
		}
		
		return "redirect:/results";
		
	}

}
