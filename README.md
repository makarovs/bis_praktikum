**Für das Projekt werden folgende Tools benutzt**:

> + Entwicklungsumgebung: Eclipse Mars (4.5)
> + UML-Modeller: Papyrus UML 1.1.4
> + Acceleo: 3.6.3
> + OCL: Standarte in UML2.* eingebettene Sprache

Das Beispiel betrachtet folgendes Scenario:
	Die Registrierung der Nutzer für WEB-Applikation:
	Es wurde einfachstes Modell namens FORM mit vier Parametern des Typen String, deren Invarianten mit RegExp definiert werden, zum ersten Release vorbereitet, um mit OCL und ihrer Transformation bekannt zu werden.
 
	Form:
		Name: nur Buchstaben erlaubt. 
		Nachname: nur Buchstaben erlaubt.
		Email (im Folgenden wird für Login neben Passwort benutzt): eine standarte Validierung der Email anhand RegExp
		Password: eine standarte Validierung des Passworts anhand RegExp;

Im Folgenden sollte die Meta-Klasse für Hibernate API aus der Form-Klasse abgeleitet werden sowie eine Fehler-Klasse, die aus den Constraints abzuleiten ist. Alles würde anhand Spring Framework funktionsfähig sein. Dieses Scenario ist noch abzusprechen.
Die RegExp Einschränkungen könnten weiter als Java Patterns transformiert und demnach für Nutzeroberfläche benutzt werden. 

Insbesonders von Interesse ist für das Projekt PIM->PSM Transformation sowie die Anpassung der bestangemessenen Ansätze bzw. Tools für ihre Implementierung. 
	 
