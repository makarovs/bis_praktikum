#bis_praktikum 

##papyrus_project

** UML-Modell**:

> Um das Modell später transformierbar z.B. in Java Kode zu werden, soll man sowohl Registered Profile > Java als auch Registered Package > JavaPrimitiveTypes im Model Explorer in RootElement importieren.
>OCL for UML wird nicht importiert, weil UML Meta-Model an sich seit 2.0 Version schon beinhaltet.
> Danach sollen die Plugins in Profile des Modelles registriert.
.di Datei > Profile > Java. Das erzeugt Pathmap, nämlich pathmap://Papyrus_JAVA_PROFILES/java.profile.uml, was im Acceleo später zu regestrieren ist. 
> Für Bequemlichkeit wird Package benutzt, unter dessem Dach Klassen erzeugt werden.
> Für die Klasse und Properties sind JavaClass Stereotyp bzw. JavaProperty zu benutzen (im Profile regestrieren). Die Visibility für die Klassen und Properties: public bzw. private. Aufgefallen ist, dass bei der Regestrierung des Propertytypes String nicht in JavaPrimitive, sondern in UMLPrimitive als EDataType herausyufinden ist.

** UML-Constraints **
> Constraints werden in UML Model definiert, und zwar für Klasse als Kontext und je per Property.

##acceleo_project

**Metamodel URIs**

>für Metamodel sind bei Erstellung des Acceleo Projektes  _http://www.eclipse.org/papyrus/JAVA/1_ und _/uml2/5.0.0/UML im Bezug aud unseres Modell zu benutzen. Später kann man es in MANIFEST.MF erweitern
>Run Configuration ist vor der Ausführung der _generate.mtl_ einzustellen
> In MANIFEST.MF/Runtime ist nameOfProject.common als Exported Package hinzufügen
 
